.. _introduction:

Introduction
============

.. contents::

Brief introduction to Tor
---------------------------

.. image:: images/1058px-How_Tor_Works_2.svg.png

Directory Authorities
----------------------

.. code-block:: none

  a special-purpose relay that maintains a list of currently-running relays
  and periodically publishes a consensus together with the other
  directory authorities.

[MetricsGlossary]_

.. image:: images/dir_auth.png

[MetricsDirAuths]_

Consensus documents
-------------------

.. code-block:: none

    a single document compiled and voted on by the directory authorities once
    per hour, ensuring that all clients have the same information about the relays
    that make up the Tor network.

[MetricsGlossary]_


.. image:: images/consensus.png

[ConsensusImg]_

Why is needed to have a consensus documents?
:::::::::::::::::::::::::::::::::::::::::::::

so that all relays have same view of the network

How is consensus created
:::::::::::::::::::::::::

#. relays publish descriptors to a directory authority (dirauth)
#. dirauths exchange descriptors with each other
#. the dirauths that have measurements from a bandwidth scanner (4/10):

   #. read those measurements
   #. calculate bandwidth weights
   #. create vote documents including the `measured` bandwidth and consensus
      weight
#. dirauths exchange votes
#. dirauths create consensus documents
#. dirauths publish consensus documents

Why is needed to have relays' bandwidth measurements (aka scanners)?
---------------------------------------------------------------------

Tor bandwidth authorities run bandwidth scanners to measure the relays capacity
in the Tor network. They send their results to the directory authorities,
which use them to calculate the bandwidth weights of the relays,
what influences how the paths between relays and built.

The bandwidth scanners play and important role on the performance and
security of the Tor network.

* to distribute the network load according to relay capacity
* relays self-advertise their bandwidths, but a malicious relay could
  self-advertise more bandwidth in order to get more circuits to it.
  Having more circuits makes easier some attacks (source routed?)

